<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'cors'], 'prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

	// Auth routes
    Route::post('auth/login', 'AuthController@login');

    Route::group(['middleware' => 'jwt.refresh'], function(){
        Route::get('auth/refresh', 'AuthController@refresh');
    });

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');

        /**
         *  User routes
         */
        Route::get('users','UserController@index');
        Route::get('users/levels','UsersLevelsController@index');
        Route::get('user/{id}','UserController@show');
        Route::post('user','UserController@store');
        Route::put('user','UserController@store');
        Route::delete('user/{id}','UserController@destroy');

        /**
         *  Authorization routes
         */
        Route::get('authorizations','AuthorizationController@index');
        Route::get('authorization/{id}','AuthorizationController@show');
        Route::post('authorization','AuthorizationController@store');
        Route::put('authorization','AuthorizationController@store');
        Route::delete('authorization/{id}','AuthorizationController@destroy');

        /**
         *  Service coordinators routes
         */
        Route::get('service_coordinators','ServiceCoordinatorController@index');
        Route::get('service_coordinator/{id}','ServiceCoordinatorController@show');
        Route::post('service_coordinator','ServiceCoordinatorController@store');
        Route::put('service_coordinator','ServiceCoordinatorController@store');
        Route::delete('service_coordinator/{id}','ServiceCoordinatorController@destroy');

        /**
         *  Workers routes
         */
        Route::get('workers','WorkerController@index');
        Route::get('worker/{id}','WorkerController@show');
        Route::post('worker','WorkerController@store');
        Route::put('worker','WorkerController@store');
        Route::delete('worker/{id}','WorkerController@destroy');

        /**
         *  Sub routes
         */
        Route::get('subs','SubController@index');
        Route::get('sub/{id}','SubController@show');
        Route::post('sub','SubController@store');
        Route::put('sub','SubController@store');
        Route::delete('sub/{id}','SubController@destroy');

	    /**
	     *  Client routes
	     */
	    Route::get('clients','ClientController@index');
	    Route::get('client/{id}','ClientController@show');
	    Route::post('client','ClientController@store');
	    Route::put('client','ClientController@store');
	    Route::delete('client/{id}','ClientController@destroy');

	    /**
	     *  CallLog routes
	     */
	    Route::get('callLogs','CallLogController@index');
	    Route::get('callLog/{id}','CallLogController@show');
	    Route::post('callLog','CallLogController@store');
	    Route::put('callLog','CallLogController@store');
	    Route::delete('callLog/{id}','CallLogController@destroy');

        /**
         *  Calendar routes
         */
        Route::get('calendar/{worker_id}/{client_id}','WorkerCalendarController@show');
        Route::post('calendar','WorkerCalendarController@store');
        Route::delete('calendar/{id}','WorkerCalendarController@destroy');
    });
});

