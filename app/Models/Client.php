<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 'last_name', 'address', 'city', 'zip_code', 'phone', 'uci_number', 'notes',
		'service_coordinator', 'parent', 'workers',
	];
}
