<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CallLog extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_at', 'user_id', 'spoke_to', 'notes'
    ];
//
//    /**
//     * The storage format of the model's date columns.
//     *
//     * @var string
//     */
//    protected $dateFormat = 'Y-m-d';
}
