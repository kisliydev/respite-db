<?php

namespace App\Http\Resources;

use App\Models\UserAccessLevel;
use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password_view,
            'access_level' => $this->access_level,
            'access_level_title' => (UserAccessLevel::find($this->access_level))->title
        ];
    }
}
