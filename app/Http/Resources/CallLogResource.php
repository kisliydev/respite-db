<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\Resource;

class CallLogResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at->format('m/d/y - H:i'),
            'user_id' => $this->user_id,
            'user_name' => User::find($this->user_id)->username,
            'spoke_to' => $this->spoke_to,
            'notes' => $this->notes
        ];
    }
}
