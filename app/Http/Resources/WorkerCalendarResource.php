<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class WorkerCalendarResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'worker_id' => $this->worker_id,
            'hours' => $this->hours,
            'miles' => $this->miles,
            'date' => $this->date
        ];
    }
}
