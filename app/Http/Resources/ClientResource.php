<?php

namespace App\Http\Resources;

use App\Models\ServiceCoordinator;
use Illuminate\Http\Resources\Json\Resource;

class ClientResource extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
	    try {
            $serviceCoordinator = ServiceCoordinator::find($this->service_coordinator);
            $service_coordinator_name = $serviceCoordinator->first_name . " " . $serviceCoordinator->last_name;
        }
        catch (\Exception $e) {
            $service_coordinator_name = '';
        }

		return [
			'id' => $this->id,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'address' => $this->address,
			'city' => $this->city,
			'zip_code' => $this->zip_code,
			'phone' => $this->phone,
			'uci_number' => $this->uci_number,
			'notes' => $this->notes,
			'service_coordinator' => $this->service_coordinator,
			'service_coordinator_name' => $service_coordinator_name,
			'parent' => $this->parent,
			'workers' => $this->workers
		];
	}
}
