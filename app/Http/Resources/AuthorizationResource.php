<?php

namespace App\Http\Resources;

use App\Models\Sub;
use Illuminate\Http\Resources\Json\Resource;

class AuthorizationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'auth_number' => $this->auth_number,
            'uci_number' => $this->uci_number,
            'sub_id' => $this->sub_id,
            'sub_title' => (Sub::find($this->sub_id))->title,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'unit' => $this->unit,
            'notes' => $this->notes,
        ];
    }
}
