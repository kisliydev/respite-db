<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\WorkerResource;
use App\Models\Worker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkerController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all workers
		return WorkerResource::collection(Worker::orderBy('last_name')->get());
    }

    /**
     * @param $id
     * @return WorkerResource
     */
    public function show($id)
    {
        //Get the WorkerResource
        $worker = Worker::findOrfail($id);

        // Return a single worker
        WorkerResource::withoutWrapping();
        return new WorkerResource($worker);
    }

    /**
     * @param $id
     * @return WorkerResource
     */
    public function destroy($id)
    {
        //Get the worker
        $worker = Worker::findOrfail($id);

        if ($worker->delete()) {
            return new WorkerResource($worker);
        }

    }

    /**
     * @param Request $request
     * @return WorkerResource|string
     */
    public function store(Request $request)
    {
        try {
            $worker = $request->isMethod('put') ? Worker::findOrFail($request->id) : new Worker;

            $worker->first_name = $request->input('first_name');
            $worker->last_name = $request->input('last_name');
            $worker->address = $request->input('address');
            $worker->city = $request->input('city');
            $worker->zip_code = $request->input('zip_code');
            $worker->phone = $request->input('phone');
            $worker->email = $request->input('email');
            $worker->payroll = $request->input('payroll');

            if($worker->save()) {
                WorkerResource::withoutWrapping();
                return new WorkerResource($worker);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }
    }
}
