<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all users
        return UserResource::collection(User::all());
    }

    /**
     * @param $id
     * @return UserResource
     */
    public function show($id)
    {
        //Get the user
        $user = User::findOrfail($id);

        // Return a single user
        UserResource::withoutWrapping();
        return new UserResource($user);
    }

    /**
     * @param $id
     * @return UserResource
     */
    public function destroy($id)
    {
        //Get the user
        $user = User::findOrfail($id);

        if($user->delete()) {
            return new UserResource($user);
        }

    }

    /**
     * @param Request $request
     * @return UserResource|string
     */
    public function store(Request $request)  {
        try {
            $user = $request->isMethod('put') ? User::findOrFail($request->id) : new User;

            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->password_view = $request->input('password');
            $user->access_level = $request->input('access_level');

            if($user->save()) {
                UserResource::withoutWrapping();
                return new UserResource($user);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }

    }
}
