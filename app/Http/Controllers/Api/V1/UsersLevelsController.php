<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\UserAccessLevel;
use App\Http\Controllers\Controller;

class UsersLevelsController extends Controller
{
    public function index()
    {
        return UserAccessLevel::all();
    }
}
