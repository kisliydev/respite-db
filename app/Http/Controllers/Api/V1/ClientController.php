<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all clients
        return ClientResource::collection(Client::all());
    }

    /**
     * @param $id
     * @return ClientResource
     */
    public function show($id)
    {
        //Get the ClientResource
        $client = Client::findOrfail($id);

        // Return a single client
        ClientResource::withoutWrapping();
        return new ClientResource($client);
    }

    /**
     * @param $id
     * @return ClientResource
     */
    public function destroy($id)
    {
        //Get the client
        $client = Client::findOrfail($id);

        if ($client->delete()) {
            return new ClientResource($client);
        }

    }

    /**
     * @param Request $request
     * @return ClientResource|string
     */
    public function store(Request $request)
    {
        try {
            $client = $request->isMethod('put') ? Client::findOrFail($request->id) : new Client;

            $client->first_name = $request->input('first_name');
            $client->last_name = $request->input('last_name');
            $client->address = $request->input('address');
            $client->city = $request->input('city');
            $client->zip_code = $request->input('zip_code');
            $client->phone = $request->input('phone');
            $client->uci_number = $request->input('uci_number');
            $client->notes = $request->input('notes');
            $client->service_coordinator = $request->input('service_coordinator');
            $client->parent = $request->input('parent');
            $client->workers = $request->input('workers');

            if($client->save()) {
                ClientResource::withoutWrapping();
                return new ClientResource($client);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }
    }
}
