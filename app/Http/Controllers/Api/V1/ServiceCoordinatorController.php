<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\ServiceCoordinatorResource;
use App\Models\ServiceCoordinator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceCoordinatorController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all serviceCoordinators
        return ServiceCoordinatorResource::collection(ServiceCoordinator::all());
    }

    /**
     * @param $id
     * @return ServiceCoordinatorResource
     */
    public function show($id)
    {
        //Get the ServiceCoordinatorResource
        $serviceCoordinator = ServiceCoordinator::findOrfail($id);

        // Return a single serviceCoordinator
        ServiceCoordinatorResource::withoutWrapping();
        return new ServiceCoordinatorResource($serviceCoordinator);
    }

    /**
     * @param $id
     * @return ServiceCoordinatorResource
     */
    public function destroy($id)
    {
        //Get the serviceCoordinator
        $serviceCoordinator = ServiceCoordinator::findOrfail($id);

        if ($serviceCoordinator->delete()) {
            return new ServiceCoordinatorResource($serviceCoordinator);
        }

    }

    /**
     * @param Request $request
     * @return ServiceCoordinatorResource|string
     */
    public function store(Request $request)
    {
        try {
            $serviceCoordinator = $request->isMethod('put') ? ServiceCoordinator::findOrFail($request->id) : new ServiceCoordinator;

            $serviceCoordinator->first_name = $request->input('first_name');
            $serviceCoordinator->last_name = $request->input('last_name');
            $serviceCoordinator->phone = $request->input('phone');
            $serviceCoordinator->email = $request->input('email');
            $serviceCoordinator->branch = $request->input('branch');

            if($serviceCoordinator->save()) {
                ServiceCoordinatorResource::withoutWrapping();
                return new ServiceCoordinatorResource($serviceCoordinator);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }
    }
}
