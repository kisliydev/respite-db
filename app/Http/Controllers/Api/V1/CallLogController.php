<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\CallLogResource;
use App\Models\CallLog;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CallLogController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        //Get all callLogs
        if (!empty($request->client_id)) {

            Client::findOrfail($request->client_id); // Check is client exist

            return CallLogResource::collection(CallLog::where('client_id', $request->client_id)->get());
        }

        return CallLogResource::collection(CallLog::all());
    }

    /**
     * @param $id
     * @return CallLogResource
     */
    public function show($id)
    {
        //Get the CallLogResource
        $callLog = CallLog::findOrfail($id);

        // Return a single callLog
        CallLogResource::withoutWrapping();
        return new CallLogResource($callLog);
    }

    /**
     * @param $id
     * @return CallLogResource
     */
    public function destroy($id)
    {
        //Get the callLog
        $callLog = CallLog::findOrfail($id);

        if ($callLog->delete()) {
            return new CallLogResource($callLog);
        }

    }

    /**
     * @param Request $request
     * @return CallLogResource|string
     */
    public function store(Request $request)
    {
        try {
            $callLog = $request->isMethod('put') ? CallLog::findOrFail($request->id) : new CallLog;

            $callLog->spoke_to = $request->input('spoke_to');
            $callLog->notes = $request->input('notes');
            $callLog->client_id = $request->input('client_id');
            $callLog->user_id = Auth::guard()->user()->id;

            if ($callLog->save()) {
                CallLogResource::withoutWrapping();
                return new CallLogResource($callLog);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return \Response::json(array(
                'code' => 500,
                'message' => $e->errorInfo[2]
            ), 500);
        }

    }
}
