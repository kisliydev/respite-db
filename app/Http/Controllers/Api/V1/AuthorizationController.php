<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\AuthorizationResource;
use App\Models\Authorization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorizationController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all authorizations
        return AuthorizationResource::collection(Authorization::all());
    }

    /**
     * @param $id
     * @return AuthorizationResource
     */
    public function show($id)
    {
        //Get the authorization
        $authorization = Authorization::findOrfail($id);

        // Return a single authorization
        AuthorizationResource::withoutWrapping();
        return new AuthorizationResource($authorization);
    }

    /**
     * @param $id
     * @return AuthorizationResource
     */
    public function destroy($id)
    {
        //Get the authorization
        $authorization = Authorization::findOrfail($id);

        if($authorization->delete()) {
            return new AuthorizationResource($authorization);
        }

    }

    /**
     * @param Request $request
     * @return AuthorizationResource|string
     */
    public function store(Request $request)  {
        try {
            $authorization = $request->isMethod('put') ? Authorization::findOrFail($request->id) : new Authorization;

            $authorization->auth_number = $request->input('auth_number');
            $authorization->uci_number = $request->input('uci_number');
            $authorization->sub_id = $request->input('sub_id');
            $authorization->start_date = $request->input('start_date');
            $authorization->end_date = $request->input('end_date');
            $authorization->unit = $request->input('unit');
            $authorization->notes = $request->input('notes');

            if($authorization->save()) {
                AuthorizationResource::withoutWrapping();
                return new AuthorizationResource($authorization);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }

    }
}
