<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\WorkerCalendarResource;
use App\Models\WorkerCalendar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkerCalendarController extends Controller
{
    /**
     * @param $worker_id
     * @param $client_id
     * @return WorkerCalendarResource|string
     */
    public function show($worker_id, $client_id)
    {
        if(empty($worker_id) || empty($client_id)){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  'Please enter valid parameters'
            ), 500);
        }

        $where = ['worker_id' => $worker_id, 'client_id' => $client_id];

        return WorkerCalendarResource::collection(WorkerCalendar::where($where)->get());
    }

    /**
     * @param $id
     * @return WorkerCalendarResource|string
     */
    public function destroy($id)
    {
        //Get the workerCalendar
        $workerCalendar = WorkerCalendar::findOrfail($id);

        if ($workerCalendar->delete()) {
            return new WorkerCalendarResource($workerCalendar);
        }
    }

    /**
     * @param Request $request
     * @return WorkerCalendarResource|string
     */
    public function store(Request $request)
    {
        try {
            $workerCalendar = new WorkerCalendar();

            $workerCalendar->client_id = $request->input('client_id');
            $workerCalendar->worker_id = $request->input('worker_id');
            $workerCalendar->hours = $request->input('hours');
            $workerCalendar->miles = $request->input('miles');
            $workerCalendar->date = $request->input('date');

            if($workerCalendar->save()) {
                WorkerCalendarResource::withoutWrapping();
                return new WorkerCalendarResource($workerCalendar);
            }
        } catch(\Illuminate\Database\QueryException $e){
            return \Response::json(array(
                'code'      =>  500,
                'message'   =>  $e->errorInfo[2]
            ), 500);
        }
    }
}
