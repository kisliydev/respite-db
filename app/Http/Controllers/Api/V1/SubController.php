<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\SubResource;
use App\Models\Sub;
use App\Models\SubLogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get all subs
        return SubResource::collection(Sub::all());
    }

    /**
     * @param $id
     * @return SubResource
     */
    public function show($id)
    {
        //Get the SubResource
        $sub = Sub::findOrfail($id);

        // Return a single sub
        SubResource::withoutWrapping();
        return new SubResource($sub);
    }

    /**
     * @param $id
     * @return SubResource
     */
    public function destroy($id)
    {
        //Get the sub
        $sub = Sub::findOrfail($id);

        if ($sub->delete()) {
            return new SubResource($sub);
        }

    }

    /**
     * @param Request $request
     * @return SubResource|string
     */
    public function store(Request $request)
    {
        try {
            if ($request->isMethod('put')) {
                $sub = Sub::findOrFail($request->id);
                $subLogs = new SubLogs();
                $subLogs->sub_id = $sub->id;
                $subLogs->old_rate = $sub->rate;
                $subLogs->new_rate = $request->input('rate');

                $user = \Tymon\JWTAuth\Facades\JWTAuth::parseToken()->authenticate();
                $subLogs->user_id = $user->id;
            } else {
                $sub = new Sub;
            }

            $sub->title = $request->input('title');
            $sub->rate = $request->input('rate');

            if ($sub->save()) {
                if (isset($subLogs)) $subLogs->save();
                SubResource::withoutWrapping();
                return new SubResource($sub);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return \Response::json(array(
                'code' => 500,
                'message' => $e->errorInfo[2]
            ), 500);
        }

    }
}
