<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('clients', function (Blueprint $table) {
		    $table->string('parent', 100);
		    $table->string('workers', 50);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('clients', function (Blueprint $table) {
		    $table->dropColumn('parent');
		    $table->dropColumn('workers');
	    });
    }
}
