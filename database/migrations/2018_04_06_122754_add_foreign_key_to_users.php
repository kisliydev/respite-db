<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
		    $table->index('access_level');
		    $table->integer('access_level')->unsigned()->nullable();
			$table->foreign('access_level')->references('id')->on('user_access_level')->onDelete('set null');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->dropForeign('access_level');
	        $table->dropIndex('access_level');
	        $table->dropColumn('access_level');
        });
    }
}
