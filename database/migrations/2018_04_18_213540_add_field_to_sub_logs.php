<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToSubLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_logs', function (Blueprint $table) {
            $table->integer('sub_id')->unsigned()->nullable();
            $table->foreign('sub_id')->references('id')->on('subs')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_logs', function (Blueprint $table) {
            $table->dropColumn('sub_id');
        });
    }
}
