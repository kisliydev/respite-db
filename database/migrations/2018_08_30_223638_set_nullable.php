<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_coordinators', function (Blueprint $table) {
			$table->string('first_name', 100)->nullable()->change();
			$table->string('last_name', 100)->nullable()->change();
			$table->string('phone', 20)->nullable()->change();
			$table->string('email', 50)->nullable()->change();
			$table->integer('branch')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_coordinators', function (Blueprint $table) {
			$table->string('first_name', 100)->change();
			$table->string('last_name', 100)->change();
			$table->string('phone', 20)->change();
			$table->string('email', 50)->change();
			$table->integer('branch')->change();
        });
    }
}
