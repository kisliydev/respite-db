<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
	        $table->text('address')->nullable()->change();
            $table->string('city', 100)->nullable()->change();
            $table->string('zip_code', 50)->nullable()->change();
            $table->string('phone', 20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
	        $table->text('address')->change();
            $table->string('city', 100)->change();
            $table->string('zip_code', 50)->change();
            $table->string('phone', 20)->change();
        });
    }
}
