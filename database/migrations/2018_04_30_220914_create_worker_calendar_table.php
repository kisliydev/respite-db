<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->index('worker_id');
            $table->integer('worker_id')->unsigned()->nullable();
            $table->foreign('worker_id')->references('id')->on('workers')->onDelete('set null');
            $table->index('client_id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_calendar');
    }
}
