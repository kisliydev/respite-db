<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('worker_calendar', function (Blueprint $table) {
            $table->string('hours', 50);
            $table->string('miles', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worker_calendar', function (Blueprint $table) {
            $table->dropColumn('hours', 50);
            $table->dropColumn('miles', 50);
        });
    }
}
