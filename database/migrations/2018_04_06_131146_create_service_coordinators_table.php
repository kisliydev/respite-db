<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCoordinatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_coordinators', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('first_name', 100);
	        $table->string('last_name', 100);
	        $table->string('phone', 20);
	        $table->string('email', 20);
	        $table->integer('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_coordinators');
    }
}
