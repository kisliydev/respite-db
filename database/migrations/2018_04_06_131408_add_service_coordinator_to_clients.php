<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceCoordinatorToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
	        $table->index('service_coordinator');
	        $table->integer('service_coordinator')->unsigned()->nullable();
	        $table->foreign('service_coordinator')->references('id')->on('service_coordinators')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
	        $table->dropForeign('service_coordinator');
	        $table->dropIndex('service_coordinator');
	        $table->dropColumn('service_coordinator');
        });
    }
}
