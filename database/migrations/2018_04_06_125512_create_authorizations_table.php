<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auth_number');
            $table->integer('uci_number');
	        $table->integer('sub_id')->unsigned()->nullable();
	        $table->foreign('sub_id')->references('id')->on('subs')->onDelete('set null');
	        $table->date('start_date');
	        $table->date('end_date');
	        $table->integer('unit');
	        $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authorizations');
    }
}
