<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\WorkerCalendar::class, function (Faker $faker) {

    $date = $faker->dateTimeBetween($startDate = '-3 months', $endDate = '+1 months');
    return [
        'worker_id' => 50,//$faker->numberBetween(\DB::table('workers')->min('id'), \DB::table('workers')->max('id')),
        'client_id' => 50,//$faker->numberBetween(\DB::table('clients')->min('id'), \DB::table('workers')->max('id')),
        'hours' =>  $faker->numberBetween(10, 99),
        'miles' =>  $faker->numberBetween(10, 999),
        'date' =>  $date->format("Y-m-d").'T21:00:00.000Z'
    ];
});
