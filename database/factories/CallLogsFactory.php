<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\CallLog::class, function (Faker $faker) {

    return [
        'user_id' => $faker->numberBetween(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        'client_id' => $faker->numberBetween(\DB::table('clients')->min('id'), \DB::table('clients')->max('id')),
        'spoke_to' => str_random(10),
        'notes' => str_random(50),
    ];
});
