<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Client::class, function (Faker $faker) {

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'address' => $faker->address,
        'city' => $faker->city,
        'zip_code' => $faker->randomNumber(6),
        'phone' => '(093) 987-7894',
        'uci_number' => $faker->randomNumber(8),
        'notes' => str_random(50),
        'service_coordinator' => $faker->numberBetween(\DB::table('service_coordinators')->min('id'), \DB::table('service_coordinators')->max('id')),
        'parent' => $faker->name,
        'workers' => implode('|',$faker->randomElements(\DB::table('workers')->where('id' ,'>' ,0)->pluck('id')->toArray(), 4)),
    ];
});
