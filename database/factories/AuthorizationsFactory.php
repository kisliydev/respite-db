<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Authorization::class, function (Faker $faker) {

    $client_id = $faker->numberBetween(\DB::table('clients')->min('id'), \DB::table('clients')->max('id'));

    return [
        'auth_number' => $faker->randomNumber(6),
        'uci_number' => \DB::table('clients')->where('id' ,'=' ,$client_id)->pluck('uci_number')->first(),
        'sub_id' => $faker->numberBetween(\DB::table('subs')->min('id'), \DB::table('subs')->max('id')),
        'start_date' => date("Y-m-d",mt_rand(1262055681,1262055681)),
        'end_date' => date("Y-m-d",mt_rand(1262055681,1262055681)),
        'unit' => $faker->randomNumber(4),
        'notes' => str_random(50),
    ];
});
