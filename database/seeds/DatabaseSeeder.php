<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserAccessLevelsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SubsTableSeeder::class);
        $this->call(WorkersTableSeeder::class);
        $this->call(ServiceCoordinatorTableSeeder::class);
        $this->call(AuthorizationsTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(CallLogsTableSeeder::class);
        $this->call(WorkersCalendarTableSeeder::class);
    }
}
