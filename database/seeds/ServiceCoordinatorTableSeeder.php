<?php

use Illuminate\Database\Seeder;

class ServiceCoordinatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ServiceCoordinator::class, 500)->create();
    }
}
