<?php

use Illuminate\Database\Seeder;

class AuthorizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Authorization::class, 500)->create();
    }
}
