<?php

use Illuminate\Database\Seeder;

class WorkersCalendarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\WorkerCalendar::class, 500)->create();
    }
}
