<?php

use Illuminate\Database\Seeder;

class UserAccessLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_access_level')->insert([
            'title' => 'Admin'
        ]);
        
        DB::table('user_access_level')->insert([
            'title' => 'Super admin'
        ]);
    }
}
