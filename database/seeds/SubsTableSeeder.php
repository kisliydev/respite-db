<?php

use Illuminate\Database\Seeder;

class SubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subs')->insert([
            'title' => 'EOR',
            'rate' => 45.00
        ]);
        DB::table('subs')->insert([
            'title' => 'MILES',
            'rate' => 22.00
        ]);
        DB::table('subs')->insert([
            'title' => 'AFS',
            'rate' => 17.12
        ]);
    }
}
