<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'test_admin',
            'email' => 'test_admin@email.com',
            'password' => bcrypt('test_admin'),
            'password_view' => 'test_admin',
            'access_level' => 1,
        ]);
        DB::table('users')->insert([
            'username' => 'test_super_admin',
            'email' => 'test_super_admin@email.com',
            'password' => bcrypt('test_super_admin'),
            'password_view' => 'test_super_admin',
            'access_level' => 2,
        ]);
    }
}
