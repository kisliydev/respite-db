import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<User[]>(environment.api_url + 'users');
    }

    getUserLevels() {
        return this.http.get<any>(environment.api_url + 'users/levels');
    }

    getById(id: number) {
        return this.http.get(environment.api_url + 'user/' + id);
    }

    create(user: User) {
        return this.http.post<User>(environment.api_url + 'user', user);
    }

    update(user: User) {
        return this.http.put<User>(environment.api_url + 'user', user);
    }

    delete(id: number) {
        return this.http.delete(environment.api_url + 'user/' + id);
    }

}
