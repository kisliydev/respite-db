import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {ServiceCoordinator} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class ServiceCoordinatorService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<ServiceCoordinator[]>(environment.api_url + 'service_coordinators');
    }

    getById(id: number) {
        return this.http.get(environment.api_url + 'service_coordinator/' + id);
    }

    create(service_coordinator: ServiceCoordinator) {
        return this.http.post<ServiceCoordinator>(environment.api_url + 'service_coordinator', service_coordinator);
    }

    update(service_coordinator: ServiceCoordinator) {
        return this.http.put<ServiceCoordinator>(environment.api_url + 'service_coordinator', service_coordinator);
    }

    delete(id: number) {
        return this.http.delete<ServiceCoordinator>(environment.api_url + 'service_coordinator/' + id);
    }

}
