import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Authorization} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthorizationService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Authorization[]>(environment.api_url + 'authorizations');
    }

    getById(id: number) {
        return this.http.get(environment.api_url + 'authorization/' + id);
    }

    create(authorization: Authorization) {
        return this.http.post<Authorization>(environment.api_url + 'authorization', authorization);
    }

    delete(id: number) {
        return this.http.delete(environment.api_url + 'authorization/' + id);
    }

}
