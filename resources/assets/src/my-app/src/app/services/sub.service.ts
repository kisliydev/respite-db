import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Sub} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class SubService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Sub[]>(environment.api_url + 'subs');
    }

    getById(id: number) {
        return this.http.get(environment.api_url + 'sub/' + id);
    }

    create(sub: Sub) {
        return this.http.post<Sub>(environment.api_url + 'sub', sub);
    }

    update(sub: Sub) {
        return this.http.put<Sub>(environment.api_url + 'sub', sub);
    }

    delete(id: number) {
        return this.http.delete(environment.api_url + 'sub/' + id);
    }

}
