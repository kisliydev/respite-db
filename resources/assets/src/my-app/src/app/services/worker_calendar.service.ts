import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {WorkerCalendar} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class WorkerCalendarService {
    constructor(private http: HttpClient) {
    }

    get(worker_id: number, client_id: number) {
        return this.http.get<WorkerCalendar[]>(environment.api_url + 'calendar/' + worker_id + '/' + client_id);
    }

    create(workerCalendar: WorkerCalendar) {
        return this.http.post<WorkerCalendar>(environment.api_url + 'calendar', workerCalendar);
    }

    delete(id: string | number) {
        return this.http.delete(environment.api_url + 'calendar/' + id);
    }

}
