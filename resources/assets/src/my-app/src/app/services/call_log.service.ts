import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {CallLog} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class CallLogService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<CallLog[]>(environment.api_url + 'callLogs');
    }

    getById(id: number) {
        return this.http.get(environment.api_url + 'callLog/' + id);
    }

    getByClientID(client_id) {
        return this.http.get(environment.api_url + 'callLogs', {params: {client_id: client_id}});
    }

    create(callLog: CallLog) {
        return this.http.post<CallLog>(environment.api_url + 'callLog', callLog);
    }

    update(callLog: CallLog) {
        return this.http.put<CallLog>(environment.api_url + 'callLog', callLog);
    }

    delete(id: number) {
        return this.http.delete(environment.api_url + 'callLog/' + id);
    }

}
