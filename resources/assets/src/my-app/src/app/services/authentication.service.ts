import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) {
    }

    login(username: string, password: string) {

        return this.http.post<any>(environment.api_url + 'auth/login', {username: username, password: password})
            .map(res => {
                // login successful if there's a jwt token in the response
                if (res.status === 'success' && res.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(res.user));
                    localStorage.setItem('currentUserAccessLevel', res.user.access_level);
                    localStorage.setItem('token', res.token);
                }

                return res;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    isAuthenticated(): boolean {
        return !(new JwtHelperService()).isTokenExpired(this.getToken());
    }

}
