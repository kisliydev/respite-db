import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Client} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class ClientService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Client[]>(environment.api_url + 'clients');
    }

    getById(id: number) {
        return this.http.get<Client>(environment.api_url + 'client/' + id);
    }

    create(client: Client) {
        return this.http.post<Client>(environment.api_url + 'client', client);
    }

    update(client: Client) {
        return this.http.put<Client>(environment.api_url + 'client', client);
    }

    delete(id: number) {
        return this.http.delete<Client>(environment.api_url + 'client/' + id);
    }

}
