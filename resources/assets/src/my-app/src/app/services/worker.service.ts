import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Worker} from '../models';
import {environment} from '../../environments/environment';

@Injectable()
export class WorkerService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Worker[]>(environment.api_url + 'workers');
    }

    getById(id: number) {
        return this.http.get<Worker>(environment.api_url + 'worker/' + id);
    }

    create(worker: Worker) {
        return this.http.post<Worker>(environment.api_url + 'worker', worker);
    }

    update(worker: Worker) {
        return this.http.put<Worker>(environment.api_url + 'worker', worker);
    }

    delete(id: number) {
        return this.http.delete<Worker>(environment.api_url + 'worker/' + id);
    }

}
