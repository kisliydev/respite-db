export class Worker {workers
    id: number;
    first_name: string;
    last_name: string;
    address: string;
    city: string;
    zip_code: string;
    phone: string;
    email: string;
    payroll: number;
}
