export class ServiceCoordinator {
    id: number;
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
    branch: number;
}
