export class Authorization {
    id: number;
    auth_number: number;
    uci_number: number;
    sub_id: number;
    start_date: string;
    end_date: string;
    unit: number;
    notes: string;
}
