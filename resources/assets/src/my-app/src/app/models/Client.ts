export class Client {
    id: number;
    first_name: string;
    last_name: string;
    address: string;
    city: string;
    zip_code: string;
    phone: string;
    uci_number: string;
    notes: number;
    service_coordinator: number;
    service_coordinator_name: string;
    parent: string;
    workers: string;
}