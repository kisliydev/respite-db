export class WorkerCalendar {
    id: number;
    worker_id: number;
    client_id: number;
    hours: string;
    miles: string;
    date: Date;
}
