export class CallLog {
    id: number;
    created_at: string;
    user_id: number;
    user_name: string;
    client_id: number;
    spoke_to: string;
    notes: string;
}