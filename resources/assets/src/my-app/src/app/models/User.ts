export class User {
    id: number;
    username: string;
    email: string;
    password: string;
    access_level: number;
    access_level_title: string;
}
