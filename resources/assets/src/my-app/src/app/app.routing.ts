import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuard } from './guards';

// Import Containers
import {
    FullLayoutComponent,
    SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'clients',
                loadChildren: './views/clients/clients.module#ClientsModule'
            },
            // {
            //     path: 'reports',
            //     loadChildren: './views/reports/reports.module#ReportsModule'
            // },
            {
                path: 'db_users',
                loadChildren: './views/db_users/db_users.module#DBUsersModule'
            },
            // {
            //     path: 'authorizations',
            //     loadChildren: './views/authorizations/authorizations.module#AuthorizationsModule'
            // },
            // {
            //     path: 'subs',
            //     loadChildren: './views/subs/subs.module#SubsModule'
            // },
            {
                path: 'workers',
                loadChildren: './views/workers/workers.module#WorkersModule'
            },
            {
                path: 'worker/:id',
                loadChildren: './views/worker/worker.module#WorkerModule'
            },
            // {
            //     path: 'time_entry',
            //     loadChildren: './views/time_entry/time_entry.module#TimeEntryModule'
            // },
            {
                path: 'service_coordinators',
                loadChildren: './views/service_coordinators/service_coordinators.module#ServiceCoordinatorsModule'
            },
            // {
            //     path: 'calendar/:worker_id/:client_id',
            //     loadChildren: './views/worker_calendar/worker_calendar.module#WorkerCalendarModule'
            // },
            {
                path: 'call_logs/:id',
                loadChildren: './views/call_logs/call_logs.module#CallLogsModule'
            },
            {
                path: 'client/:id',
                loadChildren: './views/client/client.module#ClientModule'
            }
        ],
        canActivate: [AuthGuard]
    },
    {
        path: 'system',
        component: SimpleLayoutComponent,
        data: {
            title: 'System'
        },
        children: [
            {
                path: '',
                loadChildren: './views/system/pages.module#PagesModule',
            }
        ]
    },
    { path: '**', redirectTo: 'system' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
