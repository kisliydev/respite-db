import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {DBUsersComponent} from './db_users.component';
import {DBUsersRoutingModule} from './db_users-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    imports: [
        DBUsersRoutingModule,
        NgxDatatableModule,
        NgbModule,
        NgSelectModule,
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [DBUsersComponent]
})
export class DBUsersModule {
}
