import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService, UserService} from '../../services';
import {User} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {PasswordValidation} from '../../helpers';
import {NgOption} from '@ng-select/ng-select';

@Component({
    templateUrl: 'db_users.component.html'
})
export class DBUsersComponent implements OnInit {

    page_title = 'Database users';
    users: User[];
    user_levels: NgOption[];
    userFormModal: NgbModalRef;
    deleteUserModal: NgbModalRef;
    userForm: FormGroup;
    userFormTitle: string;
    editUser: string;
    usersArrIndex;
    deleteItem = '';
    deleteIndex = null;

    constructor(
        private userService: UserService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService
    ) {
    }


    ngOnInit() {
        const currentUserAccessLevel = localStorage.getItem('currentUserAccessLevel');
        if (currentUserAccessLevel && currentUserAccessLevel !== '2') {
            this.router.navigate(['/system']);
        }

        this.getUsers();
        this.getUserAccessLevels();
    }

    add(modal) {
        this.initAddForm();
        this.userFormModal = this.modalService.open(modal);
    }

    edit(modal, user, usersArrIndex) {
        this.initEditForm(user, usersArrIndex);
        this.userFormModal = this.modalService.open(modal);
    }

    initAddForm() {
        this.userFormTitle = 'Add user';
        this.editUser = '';

        this.userForm = this.fb.group({
            id: [''],
            username: ['', [Validators.required, Validators.minLength(3)]],
            email: ['', [Validators.required, Validators.email, Validators.minLength(3)]],
            password: ['', [Validators.required, Validators.minLength(3)]],
            'password-confirm': ['', [Validators.required]],
            access_level: [null, [Validators.required]],
        }, {
            validator: PasswordValidation.MatchPassword
        });
    }

    initEditForm(user, usersArrIndex) {
        this.usersArrIndex = usersArrIndex;
        this.userFormTitle = 'User Edit';
        this.editUser = user.username;

        this.userForm = this.fb.group({
            id: [user.id],
            username: [user.username, [Validators.required, Validators.minLength(3)]],
            email: [user.email, [Validators.required, Validators.email, Validators.minLength(3)]],
            password: [user.password, [Validators.required, Validators.minLength(3)]],
            'password-confirm': [user.password, [Validators.required]],
            access_level: [user.access_level, [Validators.required]],
        }, {
            validator: PasswordValidation.MatchPassword
        });
    }

    handleUserForm() {
        const controls = this.userForm.controls;

        if (this.userForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        if (controls['id'].value !== '') {
            this.userService.update(this.userForm.value).subscribe(user => {
                    const changedIndex = this.users.findIndex(item => item.id === this.usersArrIndex);
                    this.users[changedIndex] = user;
                    this.users = [...this.users];

                    this.alertService.success('User was successfully updated!');
                    this.userFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.userService.create(this.userForm.value).subscribe(user => {
                    this.users.push(user);
                    this.users = [...this.users];

                    this.alertService.success('User was successfully created!');
                    this.userFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
    }

    delete(modal, user, usersArrIndex) {
        this.deleteIndex = this.users.findIndex(item => item.id === usersArrIndex);
        this.deleteItem = this.users[this.deleteIndex].username;
        this.usersArrIndex = usersArrIndex;
        this.deleteUserModal = this.modalService.open(modal);
    }

    acceptDeleteUser() {
        this.userService.delete(this.users[this.deleteIndex].id).subscribe(() => {
                this.users.splice(this.deleteIndex, 1);
                this.users = [...this.users];
                this.alertService.success('User was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteUserModal.close();
    }

    getUsers() {
        this.userService.getAll().subscribe(res => {
            this.users = res['data'];
        });
    }

    getUserAccessLevels() {
        this.userService.getUserLevels().subscribe(res => {
            const user_levels_formatted_arr = [];
            res.forEach(item => {
                user_levels_formatted_arr.push({
                    value: item.id,
                    label: item.title
                });
            });
            this.user_levels = user_levels_formatted_arr;
        });
    }
}
