import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DBUsersComponent} from './db_users.component';

const routes: Routes = [
    {
        path: '',
        component: DBUsersComponent,
        data: {
            title: 'Database users'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DBUsersRoutingModule {
}
