import {Component} from '@angular/core';

@Component({
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {

    page_title: 'Please select from the list below';

    constructor() {}

    isSuperAdmin() {
        const currentUserAccessLevel = localStorage.getItem('currentUserAccessLevel');
        return (currentUserAccessLevel && currentUserAccessLevel === '2');
    }
}
