import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {TimeEntryComponent} from './time_entry.component';
import {TimeEntryRoutingModule} from './time_entry-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    imports: [
        TimeEntryRoutingModule,
        NgxDatatableModule,
        NgbModule,
        NgSelectModule,
        ReactiveFormsModule,
        FormsModule,
        CommonModule
    ],
    declarations: [TimeEntryComponent]
})
export class TimeEntryModule {
}
