import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ClientService, WorkerService} from '../../services';
import {Worker, Client} from '../../models';
import {isNull} from "util";

@Component({
    templateUrl: 'time_entry.component.html'
})
export class TimeEntryComponent implements OnInit {

    page_title = 'Time entry';
    selected = [];
    clientArr: Client[];
    workerArr: Worker[];
    clients: Client[];
    subWorkerArr: Worker[];
    clientTableHidden = true;
    serachByClient = false;
    selectedWorker = null;

    constructor(
        private clientService: ClientService,
        private workerService: WorkerService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getClients();
        this.getWorkers();
    }

    change($event, type) {
        if (type === 'worker' && typeof $event !== 'undefined') {
            this.clientTableHidden = false;
            this.serachByClient = false;
            this.clients = this.clientArr.filter(function (row) {
                const workers = row.workers.split('|').map(item => parseInt(item, 10));
                return workers.indexOf($event.id) !== -1;
            });
        } else if (type === 'client' && typeof $event !== 'undefined') {
            this.clientTableHidden = false;
            this.serachByClient = true;
            this.clients = this.clientArr.filter(function (row) {
                return row.id === $event.id;
            });
            this.subWorkerArr = this.workerArr.filter(function (row) {
                return $event.workersArr.indexOf(row.id) !== -1;
            });
        } else {
            this.clientTableHidden = true;
        }
        this.clients = [...this.clients ];
    }

    selectClient({selected}) {
        if (this.serachByClient) {
            return false;
        }
        this.router.navigate(['/calendar', this.selectedWorker, selected[0].id]);
    }

    Search(term: string, item) {
        term = term.toLocaleLowerCase();
        let founded = false;
        Object.keys(item).forEach(key => {
            if (!isNull(item[key]) && item[key].toString().toLowerCase().indexOf(term) !== -1) {
                founded = true;
            }
        });

        return founded || !term;
    }

    getWorkers() {
        this.workerService.getAll().subscribe(res => {
            const workers_formatted_arr = [];
            res['data'].forEach(item => {
                item.value = item.id;
                item.label = item.first_name + ' ' + item.last_name + ' - ' + item.payroll;
                workers_formatted_arr.push(item);
            });
            this.workerArr = workers_formatted_arr;
        });
    }

    getClients() {
        this.clientService.getAll().subscribe(res => {
            const clients_formatted_arr = [];
            res['data'].forEach(item => {
                item.value = item.id;
                item.label = item.first_name + ' ' + item.last_name + ' - ' + item.uci_number;
                item.workersArr = item.workers.split('|').map(sub_item => parseInt(sub_item, 10));
                clients_formatted_arr.push(item);
            });
            this.clientArr = clients_formatted_arr;
            this.clients = clients_formatted_arr;
        });
    }
}
