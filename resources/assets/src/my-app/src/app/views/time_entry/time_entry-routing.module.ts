import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {TimeEntryComponent} from './time_entry.component';

const routes: Routes = [
    {
        path: '',
        component: TimeEntryComponent,
        data: {
            title: 'Time entry'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TimeEntryRoutingModule {
}
