import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthorizationService, CallLogService, ClientService, WorkerService} from '../../services';
import {Authorization, CallLog, Client, Worker} from '../../models';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

@Component({
    templateUrl: 'client.component.html'
})
export class ClientComponent implements OnInit {

    client_id: number;
    client: Client;
    callLogs: CallLog[];
    callLogFormModal: NgbModalRef;
    deleteCallLogModal: NgbModalRef;
    callLogForm: FormGroup;
    callLogFormTitle: string;
    callLogsArrIndex;
    deleteCallLogItem = '';
    deleteCallLogIndex = null;
    workers: Worker[];
    authorizations: Authorization[];


    page_title: string;

    constructor(
        private authorizationService: AuthorizationService,
        private callLogService: CallLogService,
        private clientService: ClientService,
        private fb: FormBuilder,
        private serviceWorkerService: WorkerService,
        private modalService: NgbModal,
        private router: Router,
        private alertService: AlertService,
        private route: ActivatedRoute
    ) {
    }


    ngOnInit() {
        this.route.params.subscribe(params => {
            this.client_id = params.id;
            this.getClient(this.client_id);
            this.getCallLogs(this.client_id);
        });
    }

    getClient(client_id) {
        this.clientService.getById(client_id).subscribe(res => {
                this.client = res;
                this.getWorkers();
                // this.getAuthorizations();
                this.setPageTitle();
            },
            () => {
                this.router.navigate(['/system/404']);
            });
    }

    getCallLogs(client_id) {
        this.callLogService.getByClientID(client_id).subscribe(res => {
            this.callLogs = res['data'];
        });
    }

    getWorkers() {
        this.serviceWorkerService.getAll().subscribe(res => {
            if(this.client.workers !== null) {
                const worker_ids = this.client.workers.split('|').map(item => parseInt(item, 10));
                this.workers = res['data'].filter((row) => worker_ids.includes(row.id));
            } else {
                this.workers = [];
            }
        });
    }

    getAuthorizations() {
        this.authorizationService.getAll().subscribe(res => {
            this.authorizations = res['data'].filter((row) => row.uci_number === this.client.uci_number);
        });
    }


    addCallLog(modal) {
        this.initAddCallLogForm();
        this.callLogFormModal = this.modalService.open(modal);
    }

    editCallLog(modal, callLog, callLogsArrIndex) {
        this.initEditCallLogForm(callLog, callLogsArrIndex);
        this.callLogFormModal = this.modalService.open(modal);
    }

    initAddCallLogForm() {
        this.callLogFormTitle = 'Add call log';
        this.callLogForm = this.fb.group({
            id: [''],
            spoke_to: ['', [Validators.required, Validators.minLength(3)]],
            notes: ['', [Validators.required]]
        });
    }

    initEditCallLogForm(callLog, callLogsArrIndex) {
        this.callLogsArrIndex = callLogsArrIndex;
        this.callLogFormTitle = 'Edit call log';
        this.callLogForm = this.fb.group({
            id: [callLog.id],
            spoke_to: [callLog.spoke_to, [Validators.required, Validators.minLength(3)]],
            notes: [callLog.notes, [Validators.required]]
        });
    }

    handleCallLogForm() {
        const controls = this.callLogForm.controls;

        if (this.callLogForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        this.callLogForm.value.client_id = this.client_id;

        if (controls['id'].value !== '') {
            this.callLogService.update(this.callLogForm.value).subscribe(callLog => {
                    const changedIndex = this.callLogs.findIndex(item => item.id === this.callLogsArrIndex);
                    this.callLogs[changedIndex] = callLog;
                    this.callLogs = [...this.callLogs];

                    this.alertService.success('Call log was successfully updated!');
                    this.callLogFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.callLogService.create(this.callLogForm.value).subscribe(callLog => {
                    this.callLogs.push(callLog);
                    this.callLogs = [...this.callLogs];

                    this.alertService.success('Call log was successfully created!');
                    this.callLogFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
    }

    deleteCallLog(modal, callLog, callLogsArrIndex) {
        this.deleteCallLogIndex = this.callLogs.findIndex(item => item.id === callLogsArrIndex);
        this.deleteCallLogItem = this.callLogs[this.deleteCallLogIndex].spoke_to;
        this.callLogsArrIndex = callLogsArrIndex;
        this.deleteCallLogModal = this.modalService.open(modal);
    }

    acceptDeleteCallLog() {
        this.callLogService.delete(this.callLogs[this.deleteCallLogIndex].id).subscribe(() => {
                this.callLogs.splice(this.deleteCallLogIndex, 1);
                this.callLogs = [...this.callLogs];
                this.alertService.success('Call log was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteCallLogModal.close();
    }


    setPageTitle() {
        this.page_title = this.client.first_name + ' ' + this.client.last_name + ' - UCI ' + this.client.uci_number;
    }
}
