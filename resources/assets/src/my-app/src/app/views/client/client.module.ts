import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {ClientComponent} from './client.component';
import {ClientRoutingModule} from './client-routing.module';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        ClientRoutingModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [ClientComponent]
})
export class ClientModule {
}
