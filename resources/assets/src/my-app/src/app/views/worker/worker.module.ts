import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {WorkerComponent} from './worker.component';
import {WorkerRoutingModule} from './worker-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        WorkerRoutingModule,
        NgxDatatableModule,
        CommonModule
    ],
    declarations: [WorkerComponent]
})
export class WorkerModule {
}
