import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService, WorkerService} from '../../services';
import {Client, Worker} from '../../models';
import {isNull} from "util";

@Component({
    templateUrl: 'worker.component.html'
})
export class WorkerComponent implements OnInit {

    worker_id: number;
    worker: Worker;
    clients: Client[];
    workers: Worker[];


    page_title: string;

    constructor(
        private clientService: ClientService,
        private serviceWorkerService: WorkerService,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }


    ngOnInit() {
        this.route.params.subscribe(params => {
            this.worker_id = params.id;
            this.getWorker(this.worker_id);
        });
    }

    getWorker(worker_id) {
        this.serviceWorkerService.getById(worker_id).subscribe(res => {
                this.worker = res;
                this.getClients();
                this.setPageTitle();
            },
            () => {
                this.router.navigate(['/system/404']);
            });
    }

    getClients() {
        this.clientService.getAll().subscribe(res => {
            this.clients = res['data'].filter((row) => {
                if(row.workers) {
                    return row.workers.split('|').map(item => parseInt(item, 10)).includes(this.worker.id);
                }
            });
        });
    }

    setPageTitle() {
        this.page_title = this.worker.first_name + ' ' + this.worker.last_name + ' - ' + this.worker.payroll;
    }
}
