import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, ClientService, ServiceCoordinatorService, WorkerService} from '../../services';
import {Client} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgOption} from '@ng-select/ng-select';
import {isNull} from 'util';

@Component({
    templateUrl: 'clients.component.html'
})
export class ClientsComponent implements OnInit {

    page_title = 'Clients';
    search: string;
    clients: Client[];
    clientsTable: Client[];
    service_coordinators: NgOption[];
    temp: Client[];
    clientFormModal_start: NgbModal;
    clientFormModal: NgbModalRef;
    deleteClientModal: NgbModalRef;
    clientForm: FormGroup;
    clientFormTitle: string;
    editClient: string;
    clientsArrIndex;
    changedIndex;
    workerArr: NgOption[];
    uciNumberModal_start: NgbModal;
    uciNumberModal: NgbModalRef;
    uciNumberModal_Number: string;
    uciNumberModal_ClientName: string;
    phoneMask = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    deleteItem = '';
    deleteIndex = null;

    constructor(private clientService: ClientService,
                private serviceCoordinatorService: ServiceCoordinatorService,
                private serviceWorkerService: WorkerService,
                private router: Router,
                private modalService: NgbModal,
                private fb: FormBuilder,
                private alertService: AlertService) {
    }


    ngOnInit() {
        this.getClients();
        this.getWorkers();
        this.getServiceCoordinators();
    }

    add(modal, uci_number_modal) {
        this.initAddForm();
        this.clientFormModal = this.modalService.open(modal);
        this.clientFormModal_start = modal;
        this.uciNumberModal_start = uci_number_modal;
    }

    edit(modal, client, clientsArrIndex, event) {
        event.stopPropagation();
        this.initEditForm(client, clientsArrIndex);
        this.clientFormModal = this.modalService.open(modal);
    }

    view_all() {
        if(this.clientsTable == undefined) {
            this.clientsTable = this.clients;
            setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 250)
        } else {
            this.clientsTable = undefined;
        }
    }

    selectClient({selected}) {
        this.router.navigate(['/client', selected[0].id]);
    }

    initAddForm() {
        this.clientFormTitle = 'Add client';
        this.editClient = '';

        // this.clientForm = this.fb.group({
        //     id: [''],
        //     first_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     last_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     address: ['', [Validators.required, Validators.minLength(3)]],
        //     city: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     zip_code: ['', [Validators.required, Validators.maxLength(50)]],
        //     phone: ['', [Validators.required, Validators.maxLength(20)]],
        //     uci_number: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(7)]],
        //     notes: [''],
        //     service_coordinator: [null, [Validators.required]],
        //     parent: ['', [Validators.required]],
        //     workers: [null, [Validators.required]]
        // });
        this.clientForm = this.fb.group({
            id: [''],
            first_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: ['', [Validators.minLength(3)]],
            city: ['', [Validators.minLength(3), Validators.maxLength(100)]],
            zip_code: ['', [Validators.maxLength(50)]],
            phone: ['', [Validators.maxLength(20)]],
            uci_number: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(7)]],
            notes: [''],
            service_coordinator: [''],
            parent: [''],
            workers: ['']
        });
    }

    get uci_number() { return this.clientForm.get('uci_number'); }

    initEditForm(client, clientsArrIndex) {
        this.clientsArrIndex = clientsArrIndex;
        this.editClient = client.first_name + " " + client.last_name;
        this.clientFormTitle = 'Client Edit';

        let workers = [];
        if(client.workers !== null) {
            workers = client.workers.split('|').map(item => parseInt(item, 10));
        }

        // this.clientForm = this.fb.group({
        //     id: [client.id],
        //     first_name: [client.first_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     last_name: [client.last_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     address: [client.address, [Validators.required, Validators.minLength(3)]],
        //     city: [client.city, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
        //     zip_code: [client.zip_code, [Validators.required, Validators.maxLength(50)]],
        //     phone: [client.phone, [Validators.required, Validators.maxLength(20)]],
        //     uci_number: [client.uci_number, [Validators.required]],
        //     notes: [client.notes],
        //     service_coordinator: [client.service_coordinator, [Validators.required]],
        //     parent: [client.parent, [Validators.required]],
        //     workers: [workers, [Validators.required]]
        // });
        this.clientForm = this.fb.group({
            id: [client.id],
            first_name: [client.first_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: [client.last_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: [client.address, [Validators.minLength(3)]],
            city: [client.city, [Validators.minLength(3), Validators.maxLength(100)]],
            zip_code: [client.zip_code, [Validators.maxLength(50)]],
            phone: [client.phone, [Validators.maxLength(20)]],
            uci_number: [client.uci_number, [Validators.required, Validators.minLength(7), Validators.maxLength(7)]],
            notes: [client.notes],
            service_coordinator: [client.service_coordinator],
            parent: [client.parent],
            workers: [workers]
        });
    }

    handleClientForm() {
        const controls = this.clientForm.controls;

        // if ((this.clientForm.value.phone).replace(/\D/g, '').length < 10) {
        //     this.clientForm.controls['phone'].setErrors({'required': true})
        // }

        if (this.clientForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        let checkUciNumberModal;
        if (controls['id'].value !== '') {
            this.changedIndex = this.temp.findIndex(item => item.id === this.clientsArrIndex);
            checkUciNumberModal = this.check_uci_number(this.clientForm.value.uci_number, this.temp[this.changedIndex].id);
        } else {
            checkUciNumberModal = this.check_uci_number(this.clientForm.value.uci_number, 0);
        }
        if (checkUciNumberModal !== undefined) {
            this.clientFormModal.close();
            this.uciNumberModal_Number = this.clientForm.value.uci_number;
            this.uciNumberModal_ClientName = checkUciNumberModal.first_name + ' ' + checkUciNumberModal.last_name;
            this.uciNumberModal = this.modalService.open(this.uciNumberModal_start);
            return false;
        }

        // Format workers to array to string with delimiter
        this.clientForm.value.workers = (this.clientForm.value.workers !== '') ? this.clientForm.value.workers.join('|')  : '';

        if (controls['id'].value !== '') {
            this.clientService.update(this.clientForm.value).subscribe(client => {
                    this.temp[this.changedIndex] = client;
                    this.clients = this.temp;
                    this.clients = [...this.clients];

                    this.alertService.success('Client was successfully updated!');
                    this.clientFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.clientService.create(this.clientForm.value).subscribe(client => {
                    this.temp.push(client);
                    this.clients = this.temp;
                    this.clients = [...this.clients];

                    this.alertService.success('Client was successfully created!');
                    this.clientFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
        this.clearSearch();
    }

    acceptUciNumberModal() {
        this.uciNumberModal.close();
        this.clientFormModal = this.modalService.open(this.clientFormModal_start);
    }

    check_uci_number(uci_number, updateClientID) {
        return this.temp.find(row => (row.uci_number === uci_number && row.id !== updateClientID));
    }

    delete(modal, clientsArrIndex, event) {
        event.stopPropagation();
        this.deleteIndex = this.temp.findIndex(item => item.id === clientsArrIndex);
        this.deleteItem = this.temp[this.deleteIndex].first_name + ' ' + this.temp[this.deleteIndex].last_name;
        this.clientsArrIndex = clientsArrIndex;
        this.deleteClientModal = this.modalService.open(modal);
    }

    call_logs_navigate(client, event) {
        event.stopPropagation();
        this.router.navigate(['/call_logs', client.id]);
    }

    acceptDeleteClient() {
        this.clientService.delete(this.temp[this.deleteIndex].id).subscribe(() => {
                this.temp.splice(this.deleteIndex, 1);
                this.clients = this.temp;
                this.clients = [...this.clients];
                this.alertService.success('Client was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteClientModal.close();
        this.clearSearch();
    }

    getClients() {
        const localStorageClients : Client[] = JSON.parse(localStorage.getItem('clients'));
        if(localStorageClients){
            console.log('Clients data received from localStorage');
            this.temp = [...localStorageClients];
            this.clients = localStorageClients;
            return;
        }

        this.clientService.getAll().subscribe(res => {
            this.temp = [...res['data']];
            this.clients = res['data'];

            console.log('set clients data to localStorage');
            localStorage.setItem('clients', JSON.stringify(res['data']));
        });
    }

    getServiceCoordinators() {
        this.serviceCoordinatorService.getAll().subscribe(res => {
            const service_coordinators_formatted_arr = [];
            res['data'].forEach(item => {
                service_coordinators_formatted_arr.push({
                    value: item.id,
                    label: item.first_name + ' ' + item.last_name
                });
            });
            this.service_coordinators = service_coordinators_formatted_arr;
        });
    }

    getWorkers() {
        const localStorageWorkers = JSON.parse(localStorage.getItem('workers_formatted'));
        if(localStorageWorkers){
            console.log('Workers formatted data received from localStorage');
            this.workerArr = localStorageWorkers;
            return;
        }

        this.serviceWorkerService.getAll().subscribe(res => {
            const workers_formatted_arr = [];
            res['data'].forEach(item => {
                workers_formatted_arr.push({
                    value: item.id,
                    label: item.first_name + ' ' + item.last_name
                });
            });
            this.workerArr = workers_formatted_arr;

            console.log('set workers formatted data to localStorage');
            localStorage.setItem('workers_formatted', JSON.stringify(workers_formatted_arr));
        });
    }

    searchFilter() {
        const val = this.search.toLowerCase();

        // update the rows and filter our data
        this.clients = this.temp.filter(function (row) {
            let founded = false;
            Object.keys(row).forEach(key => {
                try {
                    if (!isNull(row[key]) && row[key].toString().toLowerCase().indexOf(val) !== -1) {
                        founded = true;
                    }
                } catch (e) {
                    console.warn(row, e);
                }
            });

            return founded || !val;
        });

        if(val.length >= 3) {
            this.clientsTable = this.clients;
            setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 250)
        } else {
            this.clientsTable = undefined;
        }

    }

    clearSearch() {
        this.search = '';
    }

}