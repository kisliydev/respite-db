import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ClientsRoutingModule} from './clients-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {ClientsComponent} from './clients.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
    imports: [
        ClientsRoutingModule,
        NgxDatatableModule,
        NgbModule,
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        CommonModule,
        TextMaskModule
    ],
    declarations: [ClientsComponent]
})
export class ClientsModule {
}
