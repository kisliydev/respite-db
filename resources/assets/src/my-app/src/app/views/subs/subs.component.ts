import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, SubService} from '../../services';
import {Sub} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: 'subs.component.html'
})
export class SubsComponent implements OnInit{

    page_title = 'SUBs';
    subs: Sub[];
    subFormModal: NgbModalRef;
    deleteSubModal: NgbModalRef;
    subForm: FormGroup;
    subFormTitle: string;
    editSub: string;
    subsArrIndex;
    deleteItem = '';
    deleteIndex = null;

    constructor(
        private subService: SubService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService
    ) {
    }


    ngOnInit() {
        this.getSubs();
    }

    add(modal) {
        this.initAddForm();
        this.subFormModal = this.modalService.open(modal);
    }

    edit(modal, sub, subsArrIndex) {
        this.initEditForm(sub, subsArrIndex);
        this.subFormModal = this.modalService.open(modal);
    }

    initAddForm() {
        this.subFormTitle = 'Add SUB';
        this.editSub = '';

        this.subForm = this.fb.group({
            id: [''],
            title: ['', [Validators.required, Validators.minLength(3)]],
            rate: ['', [Validators.required]]
        });
    }

    initEditForm(sub, subsArrIndex) {
        this.subsArrIndex = subsArrIndex;
        this.editSub = sub.title;
        this.subFormTitle = 'SUB Edit';

        this.subForm = this.fb.group({
            id: [sub.id],
            title: [sub.title, [Validators.required, Validators.minLength(3)]],
            rate: [sub.rate, [Validators.required]],
        });
    }

    handleSubForm() {
        this.alertService.close();
        const controls = this.subForm.controls;

        if (this.subForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        this.subForm.value.rate = parseFloat(this.subForm.value.rate).toFixed(2);

        if (controls['id'].value !== '') {
            this.subService.update(this.subForm.value).subscribe(sub => {
                    const changedIndex = this.subs.findIndex(item => item.id === this.subsArrIndex);
                    this.subs[changedIndex] = sub;
                    this.subs = [...this.subs];
                    this.alertService.success('SUB was successfully updated!');
                    this.subFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.subService.create(this.subForm.value).subscribe(sub => {
                    this.subs.push(sub);
                    this.subs = [...this.subs];
                    this.alertService.success('SUB was successfully created!');
                    this.subFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
    }

    delete(modal, sub, subsArrIndex) {
        this.deleteIndex = this.subs.findIndex(item => item.id === subsArrIndex);
        this.deleteItem = this.subs[this.deleteIndex].title;
        this.subsArrIndex = subsArrIndex;
        this.deleteSubModal = this.modalService.open(modal);
    }

    acceptDeleteSub() {
        this.subService.delete(this.subs[this.deleteIndex].id).subscribe(() => {
                this.subs.splice(this.deleteIndex, 1);
                this.subs = [...this.subs];
                this.alertService.success('SUB was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteSubModal.close();
    }

    getSubs() {
        this.subService.getAll().subscribe(res => {
            this.subs = res['data'];
        });
    }
}
