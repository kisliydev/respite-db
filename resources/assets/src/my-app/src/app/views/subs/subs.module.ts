import { NgModule } from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import { SubsComponent  } from './subs.component';
import { SubsRoutingModule } from './subs-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
      SubsRoutingModule,
      NgxDatatableModule,
      NgbModule,
      ReactiveFormsModule,
      CommonModule
  ],
  declarations: [SubsComponent]
})
export class SubsModule { }
