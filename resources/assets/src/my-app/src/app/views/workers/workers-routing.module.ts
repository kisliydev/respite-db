import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { WorkersComponent } from './workers.component';

const routes: Routes = [
  {
    path: '',
    component: WorkersComponent,
    data: {
      title: 'Workers'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkersRoutingModule {}
