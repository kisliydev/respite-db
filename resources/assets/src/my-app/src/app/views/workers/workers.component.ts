import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, ClientService, WorkerService} from '../../services';
import {Client, Worker} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgOption} from '@ng-select/ng-select';
import {isNull} from "util";

@Component({
    templateUrl: 'workers.component.html'
})
export class WorkersComponent implements OnInit {

    page_title = 'Workers';
    search: string;
    workers: Worker[];
    workersTable: Worker[];
    clients: Client[];
    temp = [];
    clientArr: NgOption[];
    selectedClientArr;
    workerFormModal: NgbModalRef;
    deleteWorkerModal: NgbModalRef;
    workerForm: FormGroup;
    workerFormTitle: string;
    editWorker: string;
    workersArrIndex;
    phoneMask = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    deleteItem = '';
    deleteIndex = null;

    constructor(
        private clientService: ClientService,
        private workerService: WorkerService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService
    ) {
    }


    ngOnInit() {
        this.getClients();
        this.getWorkers();
    }

    add(modal) {
        this.initAddForm();
        this.workerFormModal = this.modalService.open(modal);
    }

    edit(modal, worker, workersArrIndex, event) {
        event.stopPropagation();
        this.initEditForm(worker, workersArrIndex);
        this.workerFormModal = this.modalService.open(modal);
    }

    view_all() {
        if(this.workersTable == undefined) {
            this.workersTable = this.workers;
            setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 250)
        } else {
            this.workersTable = undefined;
        }
    }

    selectWorker({selected}) {
        this.router.navigate(['/worker', selected[0].id]);
    }

    initAddForm() {
        this.workerFormTitle = 'Add worker';
        this.editWorker = '';

        this.workerForm = this.fb.group({
            id: [''],
            first_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: ['', [Validators.required, Validators.minLength(3)]],
            city: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            zip_code: ['', [Validators.required, Validators.maxLength(50)]],
            phone: ['', [Validators.required, Validators.maxLength(20)]],
            email: ['', [Validators.required, Validators.email, Validators.minLength(3)]],
            payroll: ['', [Validators.required]],
            clients: ['']
        });
    }

    initEditForm(worker, workersArrIndex) {
        this.workersArrIndex = workersArrIndex;
        this.editWorker = worker.first_name + " " + worker.last_name;
        this.workerFormTitle = 'Worker Edit';

        const clients = this.getWorkerClients(worker.id);
        this.selectedClientArr = (clients.length > 0) ? clients.map(item => item.id) : '';

        this.workerForm = this.fb.group({
            id: [worker.id],
            first_name: [worker.first_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: [worker.last_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: [worker.address, [Validators.required, Validators.minLength(3)]],
            city: [worker.city, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            zip_code: [worker.zip_code, [Validators.required, Validators.maxLength(50)]],
            phone: [worker.phone, [Validators.required, Validators.maxLength(20)]],
            email: [worker.email, [Validators.required, Validators.email, Validators.minLength(3)]],
            payroll: [worker.payroll, [Validators.required]],
            clients: [this.selectedClientArr]
        });
    }

    handleWorkerForm() {
        this.alertService.close();
        const controls = this.workerForm.controls;

        if ((this.workerForm.value.phone).replace(/\D/g, '').length < 10) {
            this.workerForm.controls['phone'].setErrors({'required': true})
        }

        if (this.workerForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        let client = null;

        if (controls['id'].value !== '') {
            this.workerService.update(this.workerForm.value).subscribe(worker => {
                    const changedIndex = this.temp.findIndex(item => item.id === this.workersArrIndex);
                    this.temp[changedIndex] = worker;
                    this.workers = this.temp;
                    this.workers = [...this.workers];

                    this.alertService.success('Worker was successfully updated!');
                    this.workerFormModal.close();

                    const result_client_ids = this.selectedClientArr.concat(controls['clients'].value);
                    const client_ids = result_client_ids.filter(function (item, pos) {return result_client_ids.indexOf(item) == pos});

                    client_ids.forEach(id => {
                        if (controls['clients'].value.indexOf(id) != -1 && this.selectedClientArr.indexOf(id) != -1) {
                            // element stay
                            console.log('element stay', id);
                        } else if (controls['clients'].value.indexOf(id) != -1 && this.selectedClientArr.indexOf(id) == -1) {
                            // element add
                            client = this.getClient(id);
                            console.log('element add', client);

                            let workers = [];
                            if (client.workers !== null) {
                                workers = client.workers.split('|').map(item => parseInt(item, 10));
                            }
                            workers.push(this.workerForm.value.id);
                            client.workers = (workers.length > 0) ? workers.join('|') : '';

                        } else if (controls['clients'].value.indexOf(id) == -1 && this.selectedClientArr.indexOf(id) != -1) {
                            // element delete
                            client = this.getClient(id);
                            console.log('element delete', client);

                            let workers = [];
                            if (client.workers !== null) {
                                workers = client.workers.split('|').map(item => parseInt(item, 10));
                                const indexToDelete = workers.indexOf(this.workerForm.value.id);
                                if (indexToDelete > -1) {
                                    workers.splice(indexToDelete, 1);
                                }
                                client.workers = (workers.length > 0) ? workers.join('|') : '';
                            }
                        }
                    });

                    if(client) {
                        this.clientService.update(client).subscribe(() => this.getClients(),
                            error => {
                                this.alertService.error(error.error.message);
                            });
                    }

                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.workerService.create(this.workerForm.value).subscribe(worker => {
                    this.temp.push(worker);
                    this.workers = this.temp;
                    this.workers = [...this.workers];

                    this.alertService.success('Worker was successfully created!');
                    this.workerFormModal.close();

                    controls['clients'].value.forEach(id => {
                        client = this.getClient(id);
                        console.log('element add', client);

                        let workers = [];
                        if (client.workers !== null) {
                            workers = client.workers.split('|').map(item => parseInt(item, 10));
                        }

                        workers.push(worker.id);
                        client.workers = (workers.length > 0) ? workers.join('|') : '';

                    });

                    if(client) {
                        this.clientService.update(client).subscribe(() => this.getClients(),
                            error => {
                                this.alertService.error(error.error.message);
                            });
                    }

                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
        this.clearSearch();
    }

    delete(modal, worker, workersArrIndex, event) {
        event.stopPropagation();
        this.deleteIndex = this.temp.findIndex(item => item.id === workersArrIndex);
        this.deleteItem = this.temp[this.deleteIndex].first_name + ' ' + this.temp[this.deleteIndex].last_name;
        this.workersArrIndex = workersArrIndex;
        this.deleteWorkerModal = this.modalService.open(modal);
    }

    acceptDeleteWorker() {
        this.workerService.delete(this.temp[this.deleteIndex].id).subscribe(() => {
                this.temp.splice(this.deleteIndex, 1);
                this.workers = this.temp;
                this.workers = [...this.workers];
                this.alertService.success('Worker was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteWorkerModal.close();
        this.clearSearch();
    }

    getWorkers() {
        const localStorageWorkers : Worker[] = JSON.parse(localStorage.getItem('workers'));
        if(localStorageWorkers){
            console.log('Workers data received from localStorage');
            this.temp = [...localStorageWorkers];
            this.workers = localStorageWorkers;
            return;
        }

        this.workerService.getAll().subscribe(res => {
            this.temp = [...res['data']];
            this.workers = res['data'];

            console.log('set workers data to localStorage');
            localStorage.setItem('workers', JSON.stringify(res['data']));
        });
    }

    getClients() {
        const localStorageClients = JSON.parse(localStorage.getItem('clients'));
        const localStorageClientsFormatted = JSON.parse(localStorage.getItem('clients_formatted'));
        if(localStorageClients && localStorageClientsFormatted){
            console.log('Clients data received from localStorage');
            console.log('Clients formatted data received from localStorage');
            this.clients = localStorageClients;
            this.clientArr = localStorageClientsFormatted;
            return;
        }

        this.clientService.getAll().subscribe(res => {
            const clients_formatted_arr = [];
            res['data'].forEach(item => {
                clients_formatted_arr.push({
                    value: item.id,
                    label: item.first_name + ' ' + item.last_name
                });
            });
            this.clients = res['data'];
            this.clientArr = clients_formatted_arr;

            console.log('set clients data to localStorage');
            localStorage.setItem('clients', JSON.stringify(res['data']));

            console.log('set clients formatted data to localStorage');
            localStorage.setItem('clients_formatted', JSON.stringify(clients_formatted_arr));
        });
    }

    getClient(client_id): Client {
        return this.clients.filter((row) => {
                return row.id == client_id;
        })[0];
    }

    getWorkerClients(worker_id) {
        return this.clients.filter((row) => {
            if(row.workers) {
                return row.workers.split('|').map(item => parseInt(item, 10)).includes(worker_id);
            }
        });
    }

    searchFilter() {
        const val = this.search.toLowerCase();

        // update the rows and filter our data
        this.workers = this.temp.filter(function (row) {
            let founded = false;
            Object.keys(row).forEach(key => {
                try {
                    if (!isNull(row[key]) && row[key].toString().toLowerCase().indexOf(val) !== -1) {
                        founded = true;
                    }
                } catch (e) {
                    console.warn(row, e);
                }
            });

            return founded || !val;
        });

        if(val.length >= 3) {
            this.workersTable = this.workers;
            setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 250)
        } else {
            this.workersTable = undefined;
        }
    }

    clearSearch() {
        this.search = '';
    }
}
