import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {WorkersRoutingModule} from './workers-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {WorkersComponent} from './workers.component';
import {TextMaskModule} from 'angular2-text-mask';
import {NgSelectModule} from "@ng-select/ng-select";

@NgModule({
    imports: [
        WorkersRoutingModule,
        NgxDatatableModule,
        NgbModule,
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        CommonModule,
        TextMaskModule
    ],
    declarations: [WorkersComponent]
})
export class WorkersModule {
}
