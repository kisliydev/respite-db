import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, CallLogService, ClientService} from '../../services';
import {CallLog, Client} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: 'call_logs.component.html'
})
export class CallLogsComponent implements OnInit {

    client_id: number;
    client_name: string;
    page_title: string;
    callLogs: CallLog[];
    callLogFormModal: NgbModalRef;
    deleteCallLogModal: NgbModalRef;
    callLogForm: FormGroup;
    callLogFormTitle: string;
    callLogsArrIndex;
    deleteItem = '';
    deleteIndex = null;

    constructor(
        private callLogService: CallLogService,
        private clientService: ClientService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService,
        private route: ActivatedRoute
    ) {
    }


    ngOnInit() {
        this.route.params.subscribe(params => {
            this.client_id = params.id;
            this.getCallLogs(this.client_id);
            this.setPageTitle(this.client_id);
        });
    }

    add(modal) {
        this.initAddForm();
        this.callLogFormModal = this.modalService.open(modal);
    }

    edit(modal, callLog, callLogsArrIndex) {
        this.initEditForm(callLog, callLogsArrIndex);
        this.callLogFormModal = this.modalService.open(modal);
    }

    initAddForm() {
        this.callLogFormTitle = 'Add call log for ' + this.client_name;
        this.callLogForm = this.fb.group({
            id: [''],
            spoke_to: ['', [Validators.required, Validators.minLength(3)]],
            notes: ['', [Validators.required]]
        });
    }

    initEditForm(callLog, callLogsArrIndex) {
        this.callLogsArrIndex = callLogsArrIndex;
        this.callLogFormTitle = 'Edit call log for ' + this.client_name;
        this.callLogForm = this.fb.group({
            id: [callLog.id],
            spoke_to: [callLog.spoke_to, [Validators.required, Validators.minLength(3)]],
            notes: [callLog.notes, [Validators.required]]
        });
    }

    handleCallLogForm() {
        const controls = this.callLogForm.controls;

        if (this.callLogForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        this.callLogForm.value.client_id = this.client_id;

        if (controls['id'].value !== '') {
            this.callLogService.update(this.callLogForm.value).subscribe(callLog => {
                    const changedIndex = this.callLogs.findIndex(item => item.id === this.callLogsArrIndex);
                    this.callLogs[changedIndex] = callLog;
                    this.callLogs = [...this.callLogs];

                    this.alertService.success('Call log was successfully updated!');
                    this.callLogFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.callLogService.create(this.callLogForm.value).subscribe(callLog => {
                    this.callLogs.push(callLog);
                    this.callLogs = [...this.callLogs];

                    this.alertService.success('Call log was successfully created!');
                    this.callLogFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
    }

    delete(modal, callLog, callLogsArrIndex) {
        this.deleteIndex = this.callLogs.findIndex(item => item.id === callLogsArrIndex);
        this.deleteItem = this.callLogs[this.deleteIndex].spoke_to;
        this.callLogsArrIndex = callLogsArrIndex;
        this.deleteCallLogModal = this.modalService.open(modal);
    }

    acceptDeleteCallLog() {
        this.callLogService.delete(this.callLogs[this.deleteIndex].id).subscribe(() => {
                this.callLogs.splice(this.deleteIndex, 1);
                this.callLogs = [...this.callLogs];
                this.alertService.success('Call log was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteCallLogModal.close();
    }

    getCallLogs(client_id) {
        this.callLogService.getByClientID(client_id).subscribe(res => {
            this.callLogs = res['data'];
        },
        () => {
            this.router.navigate(['/system/404']);
        });
    }

    setPageTitle(client_id) {
        this.clientService.getById(client_id).subscribe(client => {
            this.page_title = client.first_name + ' ' + client.last_name + ' Call Log - UCI ' + client.uci_number;
            this.client_name = client.first_name + ' ' + client.last_name;
        });
    }
}
