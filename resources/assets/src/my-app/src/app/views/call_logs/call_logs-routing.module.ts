import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { CallLogsComponent } from './call_logs.component';

const routes: Routes = [
  {
    path: '',
    component: CallLogsComponent,
    data: {
      title: 'Call logs'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallLogsRoutingModule {}
