import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {CallLogsComponent} from './call_logs.component';
import {CallLogsRoutingModule} from './call_logs-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        CallLogsRoutingModule,
        NgxDatatableModule,
        NgbModule,
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [CallLogsComponent]
})
export class CallLogsModule {
}
