import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService, ClientService} from '../../services';
import {Authorization} from '../../models';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthorizationService, SubService} from '../../services';
import {date_format} from '../../helpers';
import {NgOption} from '@ng-select/ng-select';
import {isNull} from 'util';

@Component({
    templateUrl: 'authorizations.component.html'
})
export class AuthorizationsComponent implements OnInit {

    page_title = 'Authorizations';
    search: string;
    authorizations: Authorization[];
    subs: NgOption[];
    clients: NgOption[];
    temp: Authorization[];
    authorizationFormModal_start: NgbModal;
    authorizationFormModal: NgbModalRef;
    authorizationExist: NgbModal;
    authorizationExistModal: NgbModalRef;
    deleteAuthorizationModal: NgbModalRef;
    authorizationForm: FormGroup;
    authorizationFormTitle: string;
    authorizationsArrIndex;
    deleteItem: number;
    deleteIndex = null;

    @ViewChild(AuthorizationsComponent) authorizationTable: AuthorizationsComponent;

    constructor(
        private authorizationService: AuthorizationService,
        private clientService: ClientService,
        private subService: SubService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService
    ) {
    }


    ngOnInit() {
        this.getAuthorizations();
        this.getSubs();
        this.getClients();
    }

    add(modal, exist_modal) {
        this.initAddForm();
        this.authorizationFormModal = this.modalService.open(modal);
        this.authorizationExist = exist_modal;
        this.authorizationFormModal_start = modal;
    }

    initAddForm() {
        this.authorizationFormTitle = 'Add authorization';
        this.authorizationForm = this.fb.group({
            auth_number: [, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
            uci_number: [null, [Validators.required]],
            sub_id: [null, [Validators.required]],
            start_date: ['', [Validators.required]],
            end_date: ['', [Validators.required]],
            unit: ['', [Validators.required]],
            notes: ['']
        });
    }

    get auth_number() { return this.authorizationForm.get('auth_number'); }

    handleAuthorizationForm() {
        const controls = this.authorizationForm.controls;
        let exist_auth_number = false;

        if (this.authorizationForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        Object.keys(this.authorizations).forEach(key => {
            if (this.authorizations[key].auth_number.toString() === this.authorizationForm.value.auth_number.toString() && !exist_auth_number) {
                this.authorizationFormModal.close();
                this.authorizationExistModal = this.modalService.open(this.authorizationExist);
                exist_auth_number = true;
            }
        });

        if (!exist_auth_number) {
            this.handleAuthorizationAdd();
        }
    }

    handleAuthorizationAdd() {
        const new_authorization = this.authorizationForm.value;
        new_authorization.start_date = date_format(this.authorizationForm.value.start_date);
        new_authorization.end_date = date_format(this.authorizationForm.value.end_date);

        this.authorizationService.create(new_authorization).subscribe(authorization => {
                this.temp.push(authorization);
                this.authorizations = this.temp;
                this.authorizations = [...this.authorizations];

                this.alertService.success('Authorization was successfully created!');
                this.authorizationFormModal.close();
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.clearSearch();
    }

    delete(modal, authorization, authorizationsArrIndex) {
        this.deleteIndex = this.temp.findIndex(item => item.id === authorizationsArrIndex);
        this.deleteItem = this.temp[this.deleteIndex].auth_number;
        this.authorizationsArrIndex = authorizationsArrIndex;
        this.deleteAuthorizationModal = this.modalService.open(modal);
    }

    acceptAuthorizationExist() {
        this.handleAuthorizationAdd();
        this.authorizationExistModal.close();
    }

    rejectAuthorizationExist() {
        this.authorizationExistModal.close();
        this.authorizationFormModal = this.modalService.open(this.authorizationFormModal_start);
    }

    acceptDeleteAuthorization() {
        this.authorizationService.delete(this.temp[this.deleteIndex].id).subscribe(() => {
                this.temp.splice(this.deleteIndex, 1);
                this.authorizations = this.temp;
                this.authorizations = [...this.authorizations];
                this.alertService.success('Authorization was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteAuthorizationModal.close();
        this.clearSearch();
    }

    getAuthorizations() {
        this.authorizationService.getAll().subscribe(res => {
            this.temp = [...res['data']];
            this.authorizations = res['data'];
        });
    }

    getSubs() {
        this.subService.getAll().subscribe(res => {
            const subs_formatted_arr = [];
            res['data'].forEach(item => {
                subs_formatted_arr.push({value: item.id, label: item.title});
            });
            this.subs = subs_formatted_arr;
        });
    }

    getClients() {
        this.clientService.getAll().subscribe(res => {
            const clients_formatted_arr = [];
            res['data'].forEach(item => {
                clients_formatted_arr.push({
                    value: item.uci_number,
                    label: item.uci_number + ' - ' + item.first_name + ' ' + item.last_name
                });
            });
            this.clients = clients_formatted_arr;
        });
    }

    searchFilter() {
        const val = this.search.toLowerCase();

        // update the rows and filter our data
        this.authorizations = this.temp.filter(function (row) {
            let founded = false;
            Object.keys(row).forEach(key => {
                try {
                    if (!isNull(row[key]) && row[key].toString().toLowerCase().indexOf(val) !== -1) {
                        founded = true;
                    }
                } catch (e) {
                    console.warn(row, e);
                }
            });

            return founded || !val;
        });
    }

    clearSearch() {
        this.search = '';
    }

}
