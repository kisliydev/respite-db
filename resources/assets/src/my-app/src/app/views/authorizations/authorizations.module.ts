import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {AuthorizationsComponent} from './authorizations.component';
import {AuthorizationsRoutingModule} from './authorizations-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    imports: [
        AuthorizationsRoutingModule,
        NgxDatatableModule,
        NgbModule,
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [AuthorizationsComponent]
})
export class AuthorizationsModule {
}
