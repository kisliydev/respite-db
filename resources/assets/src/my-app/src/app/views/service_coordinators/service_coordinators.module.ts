import {NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ServiceCoordinatorsComponent} from './service_coordinators.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {ServiceCoordinatorsRoutingModule} from './service_coordinators-routing.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
    imports: [
        ServiceCoordinatorsRoutingModule,
        NgxDatatableModule,
        NgbModule,
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        CommonModule,
        TextMaskModule
    ],
    declarations: [ServiceCoordinatorsComponent]
})
export class ServiceCoordinatorsModule {
}
