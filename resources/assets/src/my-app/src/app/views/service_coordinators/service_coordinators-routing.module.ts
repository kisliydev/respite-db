import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ServiceCoordinatorsComponent} from './service_coordinators.component';

const routes: Routes = [
    {
        path: '',
        component: ServiceCoordinatorsComponent,
        data: {
            title: 'Service coordinators'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ServiceCoordinatorsRoutingModule {
}
