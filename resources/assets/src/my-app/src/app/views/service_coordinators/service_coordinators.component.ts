import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Client, ServiceCoordinator} from '../../models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, ServiceCoordinatorService} from '../../services';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgOption} from '@ng-select/ng-select';
import {isNull} from "util";

@Component({
    templateUrl: 'service_coordinators.component.html'
})
export class ServiceCoordinatorsComponent implements OnInit {

    page_title = 'Service Coordinators';
    search: string;
    serviceCoordinators: ServiceCoordinator[];
    temp: ServiceCoordinator[];
    serviceCoordinatorFormModal: NgbModalRef;
    deleteServiceCoordinatorModal: NgbModalRef;
    serviceCoordinatorForm: FormGroup;
    serviceCoordinatorFormTitle: string;
    editServiceCoordinator: string;
    serviceCoordinatorsArrIndex;
    phoneMask = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    branches: NgOption[] = [
        { value: 1, label: '1' },
        { value: 2, label: '2' },
        { value: 3, label: '3' },
        { value: 4, label: '4' },
    ];
    deleteItem = '';
    deleteIndex = null;

    constructor(
        private serviceCoordinatorService: ServiceCoordinatorService,
        private router: Router,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private alertService: AlertService
    ) {
    }


    ngOnInit() {
        this.getServiceCoordinatorService();
    }

    add(modal) {
        this.initAddForm();
        this.serviceCoordinatorFormModal = this.modalService.open(modal);
    }

    edit(modal, serviceCoordinator, serviceCoordinatorsArrIndex) {
        this.initEditForm(serviceCoordinator, serviceCoordinatorsArrIndex);
        this.serviceCoordinatorFormModal = this.modalService.open(modal);
    }

    initAddForm() {
        this.serviceCoordinatorFormTitle = 'Add service coordinator';
        this.editServiceCoordinator = '';

        this.serviceCoordinatorForm = this.fb.group({
            id: [''],
            first_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            phone: ['', [Validators.required, Validators.minLength(3)]],
            email: ['', [Validators.required, Validators.email, Validators.minLength(3)]],
            branch: [null, [Validators.required]],
        });
    }

    initEditForm(serviceCoordinator, serviceCoordinatorsArrIndex) {
        this.serviceCoordinatorsArrIndex = serviceCoordinatorsArrIndex;
        this.editServiceCoordinator = serviceCoordinator.first_name + " " + serviceCoordinator.last_name;
        this.serviceCoordinatorFormTitle = 'Service coordinator Edit';

        this.serviceCoordinatorForm = this.fb.group({
            id: [serviceCoordinator.id],
            first_name: [serviceCoordinator.first_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            last_name: [serviceCoordinator.last_name, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            phone: [serviceCoordinator.phone, [Validators.required, Validators.minLength(3)]],
            email: [serviceCoordinator.email, [Validators.required, Validators.email, Validators.minLength(3)]],
            branch: [serviceCoordinator.branch, [Validators.required]]
        });
    }

    handleServiceCoordinatorForm() {
        this.alertService.close();
        const controls = this.serviceCoordinatorForm.controls;

        if ((this.serviceCoordinatorForm.value.phone).replace(/\D/g, '').length < 10) {
            this.serviceCoordinatorForm.controls['phone'].setErrors({'required': true})
        }

        if (this.serviceCoordinatorForm.invalid) {
            Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        if (controls['id'].value !== '') {
            this.serviceCoordinatorService.update(this.serviceCoordinatorForm.value).subscribe(serviceCoordinator => {
                    const changedIndex = this.temp.findIndex(item => item.id === this.serviceCoordinatorsArrIndex);
                    this.temp[changedIndex] = serviceCoordinator;
                    this.serviceCoordinators = this.temp;
                    this.serviceCoordinators = [...this.serviceCoordinators];

                    this.alertService.success('Service coordinator was successfully updated!');
                    this.serviceCoordinatorFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        } else {
            this.serviceCoordinatorService.create(this.serviceCoordinatorForm.value).subscribe(serviceCoordinator => {
                    this.serviceCoordinators = this.temp;
                    this.serviceCoordinators.push(serviceCoordinator);
                    this.serviceCoordinators = [...this.serviceCoordinators];
                    this.temp = this.serviceCoordinators;
                    this.alertService.success('Service coordinator was successfully created!');
                    this.serviceCoordinatorFormModal.close();
                },
                error => {
                    this.alertService.error(error.error.message);
                });
        }
        this.clearSearch();
    }

    delete(modal, serviceCoordinator, serviceCoordinatorsArrIndex) {
        this.deleteIndex = this.temp.findIndex(item => item.id === serviceCoordinatorsArrIndex);
        this.deleteItem = this.temp[this.deleteIndex].first_name + ' ' + this.temp[this.deleteIndex].last_name;
        this.serviceCoordinatorsArrIndex = serviceCoordinatorsArrIndex;
        this.deleteServiceCoordinatorModal = this.modalService.open(modal);
    }

    acceptDeleteServiceCoordinator() {
        this.serviceCoordinatorService.delete(this.temp[this.deleteIndex].id).subscribe(() => {
                this.temp.splice(this.deleteIndex, 1);
                this.serviceCoordinators = this.temp;
                this.serviceCoordinators = [...this.serviceCoordinators];

                this.alertService.success('Service coordinator was successfully deleted!');
            },
            error => {
                this.alertService.error(error.error.message);
            });
        this.deleteServiceCoordinatorModal.close();
        this.clearSearch();
    }

    getServiceCoordinatorService() {
        this.serviceCoordinatorService.getAll().subscribe(res => {
            this.temp = [...res['data']];
            this.serviceCoordinators = res['data'];
        });
    }

    searchFilter() {
        const val = this.search.toLowerCase();

        // update the rows and filter our data
        this.serviceCoordinators = this.temp.filter(function (row) {
            let founded = false;
            Object.keys(row).forEach(key => {
                try {
                    if (!isNull(row[key]) && row[key].toString().toLowerCase().indexOf(val) !== -1) {
                        founded = true;
                    }
                } catch (e) {
                    console.warn(row, e);
                }
            });

            return founded || !val;
        });
    }

    clearSearch() {
        this.search = '';
    }
}
