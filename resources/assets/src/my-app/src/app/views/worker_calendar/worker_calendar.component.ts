import {Component, Input, OnInit} from '@angular/core';
import {CalendarEvent} from 'angular-calendar';
import {Subject} from 'rxjs/Subject';
import {isSameDay, isSameMonth} from 'date-fns';
import {AlertService, AuthorizationService, ClientService, WorkerCalendarService, WorkerService} from '../../services';
import {ActivatedRoute, Router} from '@angular/router';
import {Client, Worker, WorkerCalendar} from '../../models';
import {ContextMenuComponent, ContextMenuService} from 'ngx-contextmenu';
import {isNull} from 'util';

@Component({
    templateUrl: 'worker_calendar.component.html'
})
export class WorkerCalendarComponent implements OnInit {

    page_title: string;
    page_additional_title: string;
    worker_id: number;
    client_id: number;
    workerData: Worker;
    clientData: Client;

    months_arr: string[] = ['January', 'February', 'March', 'April', 'May', 'June',
        'Jule', 'August', 'September', 'October', 'November', 'December'];
    view = 'month';
    currentMonth: number;
    allowMiles = false;
    totalHours = 0;
    viewDate: Date = new Date();
    viewDateOriginal: Date = new Date();
    activeDayIsOpen = false;
    events: CalendarEvent[] = [];
    refresh: Subject<any> = new Subject();
    hours: string = null;
    miles: string = null;
    @Input() contextMenu: ContextMenuComponent;

    constructor(
        private clientService: ClientService,
        private workerService: WorkerService,
        private workerCalendarService: WorkerCalendarService,
        private authorizationService: AuthorizationService,
        private alertService: AlertService,
        private contextMenuService: ContextMenuService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.worker_id = params.worker_id;
            this.client_id = params.client_id;
            this.getWorker(this.worker_id);
            this.getClient(this.client_id);
            this.getEvents(this.worker_id, this.client_id);
            this.viewDateOriginal = this.viewDate;
        });
        setInterval(() => {
            if (this.viewDateOriginal !== this.viewDate) {
                this.setTotal();
                this.viewDateOriginal = this.viewDate;
            }
        }, 500);
    }

    addEvent($event: MouseEvent, date: Date) {
        if (this.hours === '' || this.hours === null || ((this.miles === '' || this.miles === null) && this.allowMiles)) {
            this.alertService.error('Please input correct data!');
            this.hours = this.miles = '';
            return false;
        }

        const data: WorkerCalendar = new WorkerCalendar();
        data.client_id = this.client_id;
        data.worker_id = this.worker_id;
        data.hours = this.hours;
        data.miles = isNull(this.miles) || this.miles == '' ? '0' : this.miles;
        data.date = date;

        this.workerCalendarService.create(data).subscribe(workerCalendar => {
            this.events.push({
                id: workerCalendar.id,
                title: (workerCalendar.miles !== '0') ? workerCalendar.hours + ' hours ' + workerCalendar.miles + ' miles' : workerCalendar.hours + ' hours ',
                start: new Date(workerCalendar.date),
                actions: [
                    {
                        label: '<i class="fa fa-fw fa-times"></i>',
                        onClick: ({event}: { event: CalendarEvent }): void => {
                            this.delEvent(event);
                        }
                    }
                ],
                meta: {
                    hours: workerCalendar.hours,
                    miles: workerCalendar.miles
                }
            });
            this.alertService.success('Event was successful added!');
            this.hours = this.miles = '';
            this.setTotal();
            this.activeDayIsOpen = false;
            this.refresh.next();
        },
        error => {
            this.alertService.error(error.error.message);
        });
        this.closeAllContextMenus($event);
    }

    delEvent(event: CalendarEvent) {
        this.workerCalendarService.delete(event.id).subscribe(res => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.alertService.success('Event was successful deleted!');
                this.setTotal();
                this.activeDayIsOpen = false;
                this.refresh.next();
            },
            error => {
                this.alertService.error(error.error.message);
            });
    }

    getClient(client_id) {
        this.clientService.getById(client_id).subscribe(res => {
                this.clientData = res;
                this.page_additional_title = 'CLIENT: ' + res.first_name + ' ' + res.last_name + ' ' + res.uci_number;
                this.checkAuthorization(this.clientData);
            },
            () => {
                this.router.navigate(['/system/404']);
            });
    }

    dayClicked($event: MouseEvent, {date, events}: { date: Date; events: CalendarEvent[] }, contextMenu): void {
        $event.preventDefault();
        $event.stopPropagation();
        if (isSameMonth(date, this.viewDate)) {
            if (contextMenu) {
                this.contextMenuService.show.next({
                    event: $event,
                    item: date,
                    contextMenu: this.contextMenu
                });
                return;
            } else {
                if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                    this.activeDayIsOpen = false;
                } else {
                    this.activeDayIsOpen = true;
                    this.viewDate = date;
                }
            }
        }
        this.closeAllContextMenus($event);
    }

    getWorker(worker_id) {
        this.workerService.getById(worker_id).subscribe(res => {
                this.workerData = res;
                this.page_title = 'Time entry - ' + res.first_name + ' ' + res.last_name + ' ' + res.payroll;
            },
            () => {
                this.router.navigate(['/system/404']);
            });
    }

    getEvents(worker_id, client_id) {
        this.events = [];
        this.refresh.next();
        this.workerCalendarService.get(worker_id, client_id).subscribe(res => {
                res['data'].forEach(item => {
                    this.events.push({
                        id: item.id,
                        title: (item.miles !== '0') ? item.hours + ' hours ' + item.miles + ' miles' : item.hours + ' hours ',
                        start: new Date(item.date),
                        actions: [
                            {
                                label: '<i class="fa fa-fw fa-times"></i>',
                                onClick: ({event}: { event: CalendarEvent }): void => {
                                    this.delEvent(event);
                                }
                            }
                        ],
                        meta: {
                            hours: item.hours,
                            miles: item.miles
                        }
                    });
                });
                this.setTotal();
                this.refresh.next();
            },
            error => {
                this.alertService.error(error.error.message);
            });
    }

    checkAuthorization(client: Client) {
        this.refresh.next();
        this.authorizationService.getAll().subscribe(res => {
                const self = this;
                res['data'].some(function (value) {
                    if (value.sub_title === 'MILES' && value.uci_number === client.uci_number) {
                        self.allowMiles = true;
                        return true;
                    }
                    return false;
                });
                this.refresh.next();
            },
            error => {
                this.alertService.error(error.error.message);
            });
    }

    closeAllContextMenus($event) {
        this.contextMenuService.closeAllContextMenus({
            eventType: 'cancel',
            event: $event
        });
    }

    setTotal() {
        this.currentMonth = this.viewDate.getUTCMonth();
        this.totalHours = 0;
        this.events.forEach(item => {
            if ((new Date(item.start)).getUTCMonth() === this.viewDate.getUTCMonth()) {
                this.totalHours = this.totalHours + parseInt(item.meta.hours, 10);
            }
        });
    }
}