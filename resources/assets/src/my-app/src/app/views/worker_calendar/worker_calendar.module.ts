import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {CalendarModule} from 'angular-calendar';
import {ContextMenuModule} from 'ngx-contextmenu';
import {DemoUtilsModule} from './calendar_components/module';
import {WorkerCalendarRoutingModule} from './worker_calendar-routing.module';
import {WorkerCalendarComponent} from './worker_calendar.component';

@NgModule({
    imports: [
        WorkerCalendarRoutingModule,
        CommonModule,
        CalendarModule.forRoot(),
        FormsModule,
        ContextMenuModule.forRoot({
            useBootstrap4: true,
            autoFocus: true,
        }),
        DemoUtilsModule
    ],
    declarations: [WorkerCalendarComponent]
})
export class WorkerCalendarModule {
}
