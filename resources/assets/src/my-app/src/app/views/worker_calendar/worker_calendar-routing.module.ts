import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {WorkerCalendarComponent} from './worker_calendar.component';

const routes: Routes = [
    {
        path: '',
        component: WorkerCalendarComponent,
        data: {
            title: 'Calendar'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkerCalendarRoutingModule {
}
