import { NgModule } from '@angular/core';

import { P404Component } from './404.component';
import { LoginComponent } from './login.component';

import { PagesRoutingModule } from './pages-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [ PagesRoutingModule, CommonModule, FormsModule ],
  declarations: [
    P404Component,
    LoginComponent,
  ]
})
export class PagesModule { }
