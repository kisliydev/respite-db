export function date_format(date_picker_object) {
    const date = new Date( date_picker_object.year, date_picker_object.month, date_picker_object.day);
    return date.getFullYear() + '-' + ('0' + (date.getMonth())).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
}
