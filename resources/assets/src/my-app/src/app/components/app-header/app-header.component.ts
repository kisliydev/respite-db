import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services';

@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html'
})
export class AppHeaderComponent implements OnInit {

    public navbar_title = 'Respite inc.';

    public menu_list = [
        {title: 'Clients', link: 'clients'},
        // {title: 'Reports', link: 'reports'},
        // {title: 'Authorizations', link: 'authorizations'},
        // {title: 'SUBs', link: 'subs'},
        {title: 'Workers', link: 'workers'},
        // {title: 'Time entry', link: 'time_entry'},
        {title: 'Service coordinators', link: 'service_coordinators'}
    ];

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

       ngOnInit() {
        const currentUserAccessLevel = localStorage.getItem('currentUserAccessLevel');
           if (currentUserAccessLevel && currentUserAccessLevel === '2') {
               this.menu_list.splice(2, 0, {title: 'Database users', link: 'db_users'});
           }
       }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/system/login']);
    }
}
